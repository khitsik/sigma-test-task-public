<?php
		
$matrix = array();
$userInput = $_POST['value'];
$matrixSize=$userInput*$userInput;

if (isset($userInput)) {
    $matrix = matrixWithoutMinuses($userInput, $matrixSize);
}

/**
 * Main function. Building matrix and plusing a correct
 * number to the initial value
 * so that there are no zero and negative values.
 *
 * @param {int} $userInput Number inputed by user.
 * @param {int} $matrixSize Quantity of matrix elements.
 * @return {Array} $matrix Ready to go matrix.
 */
function matrixWithoutMinuses($userInput, $matrixSize) {
    $matrix = buildSpiralMatrix($userInput,$matrixSize);
    $countMinuses = 0;

    for ($d = 0; $d < 20; $d++) { 

        foreach ($matrix as $value) {
            foreach ($value as $value2) {
                if ($value2 <= 0) {
                    $countMinuses++;
                }
            }
        }

        $matrixSize += $countMinuses;
        $countMinuses = 0;
        $matrix = buildSpiralMatrix($userInput, $matrixSize);
    }

    if ($userInput % 2 == 1) {
        $matrixSize += 1;
        $matrix = buildSpiralMatrix($userInput, $matrixSize);
    }

    return $matrix;
}

/**
 * Building matrix in a spiral
 * without values that contain sixes.
 * (But it contains negative values)
 *
 * @param {int} $userInput Number inputed by user.
 * @param {int} $matrixSize Quantity of matrix elements.
 * @return {Array} $matrix Matrix built in a spiral without sixes.
 */
function buildSpiralMatrix($userInput, $matrixSize) {
    $middleValue = $userInput / 2;

    for ($k = 1; $k <= $middleValue; $k++) {/*Cycle by round number*/

        for ($j = $k-1; $j < $userInput-$k+1; $j++) {
            $matrix[$j][$k-1] = skipSixes($matrixSize); /* --//-- On the left vertical column*/
            $matrixSize--;
        }

        for ($j = $k; $j < $userInput-$k+1; $j++) {
            $matrix[$userInput-$k][$j] = skipSixes($matrixSize); /* --//-- On the bottom horizontal field*/
            $matrixSize--;
        }

        for ($j = $userInput-$k-1; $j >= $k-1; $j--) {
            $matrix[$j][$userInput-$k] = skipSixes($matrixSize); /* --//-- On the right vertical column*/
            $matrixSize--;
        }

        for ($j = $userInput-$k-1; $j >= $k; $j--) {
            $matrix[$k-1][$j] = skipSixes($matrixSize); /* --//-- On the upper horizontal field*/
            $matrixSize--;
        }
    }

    if ($userInput % 2 == 1) {
        $matrix[$middleValue][$middleValue] = 1;
    }

    foreach ($matrix as $key => &$value) {
        ksort($value);
    }
    
    return $matrix;
}

/**
 * Takes a number and decrease it until
 * it will not contain sixes.
 *
 * @param {int} $int Number in a matrix.
 * @return {int} $int Number without sixes.
 */
function skipSixes(&$int) {
    while (strstr($int, '6')) {
        $int--;
    }

    return $int;
}