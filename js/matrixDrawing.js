var drawingCanvas = document.getElementById('smile');

var context = drawingCanvas.getContext('2d');
var x = 550;
var y = 250;
var timer = 0;

context.textAlign = 'center';
context.font = '12px Arial';
context.fillStyle = 'black';

var speedArr = window.location.hash.split('=');
var speed = speedArr[1];

/*
 * Changing speed when # variable changes.
 * Validation implemented.
 */
$(window).on('hashchange', function() {
    speedArr = window.location.hash.split('=');
    var speedCheck = parseInt(speedArr[1]);

    if (speedCheck < 100 || speedCheck > 2000 || !Number.isInteger(speedCheck)) {
        alert('Animation speed must be greater '+
              'than 100 and less than 2000');
        window.location.hash = 'animation_speed=500';
    } else {
        speed = speedArr[1];
    }
});

/**
 * Drawing matrix in a spiral with speed delay
 *
 * @param {number} speed Value from url (can be 100-2000).
 * @param {number} timer Just counter to increase timer.
 * @param {number} x Start position of X asix.
 * @param {number} y Start position of Y asix.
 */
function drawMatrix(speed, timer, x, y) {
    for(var k = 1; k <= middleValue; k++) {

        for (var j = k-1; j < userInput-k+1; j++) {
            setTimeout(function(x, y, num) {
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.fillText(num, x + 25, y + 28, 50);
                }.bind(this, x, y, matrix[j][k-1]), (timer++) * speed);
            y = y + 50;
        }

        y = y - 50;
        x = x + 50;

        for (var j = k; j < userInput-k+1; j++) {
            setTimeout(function(x, y, num) {
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.fillText(num, x + 25, y + 28, 50);
                }.bind(this, x, y, matrix[userInput-k][j]), (timer++) * speed);
            x = x + 50;
        }

        x = x - 50;
        y = y - 50;

        for (var j = userInput-k-1; j >= k-1; j--) {
            setTimeout(function(x, y, num) {
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.fillText(num, x + 25, y + 28, 50);
                }.bind(this, x, y, matrix[j][userInput-k]), (timer++) * speed);
            y = y - 50;
        }

        y = y + 50;
        x = x - 50;

        for (var j = userInput-k-1; j >= k; j--) {
            setTimeout(function(x, y,num) {
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.fillText(num, x + 25, y + 28, 50);
                }.bind(this, x, y, matrix[k-1][j]), (timer++) * speed);
            x = x - 50;
        }

        x = x + 50;
        y = y + 50;
    }

    if (userInput % 2 == 1) {
        setTimeout(function(x, y, num) {
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.strokeRect(x, y, 50, 50);
                    context.fillText(num, x + 25, y + 28, 50);
        }.bind(this, x, y, matrix[middleValue][middleValue]), (timer++) * speed);
    }
}