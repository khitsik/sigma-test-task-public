$('#smile').hide();

$( document ).ready(function() {
    $("#form").submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "index.php",
                data: $(this).serialize()
            }).done(function () {
                $("#form").hide();
                $("#smile").show();
            });
            
            return false;
    });
});

function call() {
    var msg = $('#form2').serialize();
    $.ajax({
        type: 'POST',
        url: 'index.php',
        data: msg,
        async: false,
        success: function(data) {
        $('#result_form').html(data);
        },
        error:  function(xhr, str){
        alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
}