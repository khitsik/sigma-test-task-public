Building matrix 
=====================

### User inputs a number "X" and clicks "GO" button. Then form disappears and
### it starts to draw X*X matrix in a spiral by decrease without numbers that contain sixes.

File name           | File content
--------------------|----------------------
index.php           | Main file
MatrixBuilding.php  | Logic of matrix building
.htaccess           | Simple htaccess file
js/matrixDrawing.js | Here is the matrix drawing magic
js/ajax.js          | Ajax query file
css/style.css       | Cascading style sheet file that collects the required styles