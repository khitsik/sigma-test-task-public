﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Test Task</title>

        <link rel="shortcut icon" href="images/faviconka_ru_12.ico" type="image/x-icon">

        <link type="text/css" rel="stylesheet" href="http://localhost/sigma-test-task/css/style.css"/>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id="wholePage">
            <div id="form">

                <form id="form2" method="post" action="javascript:void(null);" onsubmit="call()">
                    <p><b>Введите число</b></p>
                    <p><input type="number" class="form-control" name="value" min="1" max="1000" required></p>
                    <button type="submit" id="button" class="btn btn-info btn-md">GO</button>
                </form>

                <?php 
                    require_once 'MatrixBuilding.php';
                ?>
            </div>
        </div>

        <canvas id="smile" width="10000" height="10000">

        <div id="result_form"></div>

        <script type="text/javascript">
            <?php if (isset($userInput)): ?>

                var userInput = <?= $userInput ?>;
                var matrix = <?= json_encode($matrix); ?>;
                var middleValue = parseInt(userInput / 2);
                drawMatrix(speed, timer, x, y);

            <?php else: ?>
                window.location.hash = 'animation_speed=500';
            <?php endif ?>
        </script>
        <script src="http://localhost/sigma-test-task/js/ajaxQuery.js"></script>
        <script src="http://localhost/sigma-test-task/js/matrixDrawing.js"></script>
    </body>
</html>


